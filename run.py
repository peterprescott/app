"""
Serve API using Flask.
"""

import os                               # import files safely for any os
import sys
import json                             # for parsing javascript object notation
from flask import Flask, jsonify, request  # for serving web app
from flask_cors import CORS             # stop CORS errors
from passlib.hash import pbkdf2_sha256  # encrypted passwords
from datetime import datetime, timedelta
import python_jwt as jwt, jwcrypto.jwk as jwk
import webbrowser                       # to load static gui without hassle

import db_commands as db

webbrowser.open(os.path.join('static','index.html'))

q = db.Query(os.path.join(sys.path[0], 'new.db'))

app = Flask(__name__)
CORS(app)

JWT_security_key = jwk.JWK.generate(kty='RSA', size=2048)


def book_json(key, book=False, author=False, publisher=False):
    """Return book data for given key."""
    
    if book:
        pass
    else:
        try:
            book = q.search('books', 'key', key, with_rowid = False, precise=True)[0]
        except IndexError:
            print(f'Could not find data for book with key {key}')
            return {'status':'error'}
    book_json ={'key':key, 'title':book[1], 'location':book[2], \
    'dateAdded':book[3], 'publicationDate':book[4], 'pages':book[5]}

        # get author
    if author:
        pass
    else:
        try:
            creator_key = q.search('creator_books', 'item_id', key, with_rowid = False, precise=True)[0][2]
            creator = q.search('creator', 'key', creator_key, with_rowid = False, precise=True)[0]
            author = creator[2] + ' ' + creator[1]
        except:
            author = 'Author Not Found'
        
    book_json.update({'author':author})

    # get publisher
    if publisher:
        pass
    else:
        try:
            publisher_key = q.search('publisher_item', 'item_id', key, with_rowid = False, precise=True)[0][2]
            publisher = q.search('publishers', 'key', publisher_key, with_rowid = False, precise=True)[0][1]
        except IndexError:
            publisher = ''
        
    book_json.update({'publisher':publisher})
    
    
    # get value
    # get barcode
    
    
    return book_json
    

@app.route('/add', methods = ['POST'])
def add():
    """Add data for new item."""
    
    token = request.json['token']
    print('verifying token')
    # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
    except:
        return jsonify({"status":'Invalid session token'})    
    
        # then confirm admin role
    is_admin = q.search('admins', 'email', email)
    
    if len(is_admin) > 0:
        if is_admin[0][2] == 'admin':
            role = 'admin'
    else:
        return jsonify({'status':'access denied'})
        
    data = request.json
    
    book_id = data['scannedISBN']
    title = data['title']
    location = data['location']
    date_added = data['accessDate']
    publishedDate = data['date']
    pages = data['numPages']
    
    book_row = (book_id, title, location, date_added, publishedDate, pages)
    q.save_row_to_table('books', book_row)
    
    creators = data['creators']
    for creator in creators:
        last_name = creator['lastName'].split(',')[0]
        if len(creator['firstName']) > 0:
            other_names = creator['firstName']
        else:
            try:
                other_names = creator['lastName'].split(',')[1]
            except IndexError:
                other_names = ""
        # ~ creator_id = search for creator of that name, and match, else create new
    
    publisher = data['publisher']

    match_creator = q.search('creator', 'last_name', last_name)
    if len(match_creator) == 0:
        # ie. no matches
        creator_id = last_name + book_id
        new_creator = (creator_id, last_name, other_names)
        q.save_row_to_table('creator', new_creator)
    else:
        matched = False
        for match in match_creator:
            if match[2] == other_names:
                creator_id = match[0]
                matched = True
                break
        if matched == False:
            creator_id = last_name + book_id
            new_creator = (creator_id, last_name, other_names)
            q.save_row_to_table('creator', new_creator)
    
    creator_books_row = (creator_id + book_id, book_id, creator_id)
    
    q.save_row_to_table('creator_books', creator_books_row)        
    
    # publihser_id ~ search for publisher of that name and match, else create new.
    match_publisher = q.search('publishers', 'name', publisher, precise = True)
    if len(match_publisher) == 0:
        # ie. no matches
        publisher_id = publisher + book_id
    else:
        match = match_publisher[0]
        publisher_id = match[0]

    new_publisher = (publisher_id, publisher)
    q.save_row_to_table('publishers', new_publisher)

    publisher_book_row = (publisher_id + book_id, book_id, publisher_id)
    
    q.save_row_to_table('publisher_item', publisher_book_row)        
    
    return jsonify({'message':'success!'})

@app.route('/transactions/<token>/<action>/<item>')
def transact(token, action, item):
    """Make given transaction for given (verified) account and given item."""
    
    print('verifying token')
    # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
    except:
        return jsonify({"status":'Invalid session token'})
    
    now = datetime.now().strftime("%Y-%m-%d.%H:%M.")
    
    if action == 'request':
        # check availability
        item_records = q.search('transactions', 'item', item)
        
        availability = 'unknown'
        for item_record in item_records:
            if item_record[5] == 'unfinished':
                availability = 'unavailable'
        
        
        if availability == 'unavailable':
            return jsonify({'status':'item not available'})
        else:
            record = (
                item+now, 
                email, 
                item, 
                'requested',
                now, 
                'unfinished',
                )
            return jsonify(q.save_row_to_table('transactions', record))
    
    if action == "cancel" or action == "issue":
        # find transaction for 'user', 'item', 'requested', 'unfinished'
        item_records = q.search('transactions', 'item', item, with_rowid=True)
    
        for item_record in item_records:
            if item_record[6] == 'unfinished':
                old_record = list(item_record)
                old_record[6] = now
                q.remove_row('transactions', item_record[0])
                break
        
        old_record.pop(0)
        q.save_row_to_table('transactions', old_record)  
        
        if action == 'issue':
            action = 'borrowed'
            finish = 'unfinished'
        elif action == 'cancel':
            finish = now

        
        new_record = (
                    old_record[0]+"1",
                    old_record[1],
                    old_record[2],
                    action,
                    now,
                    finish,        
                    )

        q.save_row_to_table('transactions', new_record)
                    
        return jsonify({'status':'200'})
        
    if action == "return":

    # find transaction for 'user', 'item', 'requested', 'unfinished'
        item_records = q.search('transactions', 'item', item, with_rowid=True)
    
        for item_record in item_records:
            if item_record[6] == 'unfinished':
                old_record = list(item_record)
                old_record[6] = now
                q.remove_row('transactions', item_record[0])
                break
        
        old_record.pop(0)
        q.save_row_to_table('transactions', old_record)                    
                    
        return jsonify({'status':'200'})

@app.route('/search/<value>')
def search(value):
    """Search for title."""
    
    # ~ allowed = ['author', 'isbn', 'title', 'location']
    # ~ if field not in allowed:
        # ~ return jsonify('Sorry, that doesn\'t work')
    

    # search for title
    book_data = []
    author_book_data = []
    publisher_book_data = []
    
    
    book_results = q.search('books', 'title', value)
    print(book_results)
    for book in book_results:
        key = book[0]
        book_data.append(book_json(key, book=book))
        
        
    # search for author
    
    last_name = value.split(' ')[-1]
    author_results = q.search('creator', 'last_name', last_name)
    print(author_results)
    for author in author_results:
        author_name = author[2] + ' ' + author[1]

        author_books_id = q.search('creator_books', 'creator_id', author[0], precise=True)
        for books_id in author_books_id:
            
            author_books = q.search('books', 'key', books_id[1], precise=True)
            for book in author_books:
                key = book[0]
                author_book_data.append(book_json(key, book=book, author=author_name))

    # ~ # search for publisher
    
    publisher_results = q.search('publishers', 'name', value)
    print(publisher_results)
    for publisher in publisher_results:
        publisher_id = publisher[0]
        publisher_item_keys = q.search('publisher_item', 'publisher_id', publisher_id, precise=True)
        for publisher_item_key in publisher_item_keys:
            key = publisher_item_key[1]
            publisher_books = q.search('books', 'key', key, precise=True)
            for book in publisher_books:
                publisher_book_data.append(book_json(key, book=book, publisher=publisher[1]))
    
    biggest = max(len(x) for x in [book_data, author_book_data, publisher_book_data])
    
    book_data += author_book_data + publisher_book_data
    
    return jsonify(book_data)
    
    # ~ if len(book_data) == biggest:
        # ~ return jsonify(book_data)
    # ~ elif len(author_book_data) == biggest:
        # ~ return jsonify(author_book_data)
    # ~ elif len(publisher_book_data) == biggest:
        # ~ return jsonify(publisher_book_data)
    

@app.route('/transactions/<token>', methods = ['GET'])
def transactions(token):
    """If admin, return transaction data for requested and borrowed books."""
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
    except:
        return jsonify({'status':401,'message':'JSON web token could not be verified.'})
    
    
        # then confirm admin role
    is_admin = q.search('admins', 'email', email)
    
    if len(is_admin) > 0:
        if is_admin[0][2] == 'admin':
            role = 'admin'
    else:
        role = 'user'
    
    if role == 'admin':
        # get transaction data
        requested = []
        borrowed = []
        
        records = q.search('transactions', 'fin_time', 'unfinished')
        for record in records:
            item_id = record[2]
            item_data = q.search('books', 'key', item_id)
            try:
                book_title = item_data[0][1]
            except IndexError:
                book_title = 'Could not be found'
            # ~ value_data = q.search('value', 'item_id', item_id)
            # ~ value = value_data[3]
            # for the meantime
            value = 5
            status = record[3]
            person_id = record[1]
            person_data = q.search('accounts', 'key', person_id)
            if len(person_data) == 1:
                person_data = person_data[0]
            else:
                person_data = (0,1,2,3,4,'not found',6,7,8)
            ## person_cols ~ 0'key', 1'permission', 2'phone', 3'email', 
            ## 4'postcode', 5'first_name', 6'last_name', 7'date_of_birth'
            first_name = person_data[5]
            last_name = person_data[6]
            phone = person_data[2]
            record_data = {
                            'transaction': record[0],
                            'item_id': item_id,
                            'person': person_data,
                            'phoneNumber': phone,
                            'title': book_title,
                            'value': value,
                            }
            if status == 'requested':
                requested.append(record_data)
            elif status == 'borrowed':
                borrowed.append(record_data)
        
        requested_number = len(requested)
        borrowed_number = len(borrowed)

        
        return jsonify({
            'status':200, 
            'message':'Request to see transaction data was successful.', 
            'requested':requested,
            'requested_number': requested_number,
            'borrowed_number': borrowed_number,
            'borrowed':borrowed,
            })
    else:
        return jsonify({'status': 401, 'message':'Account does not have administrator privileges.'})

   

@app.route('/login', methods = ['POST'])
def login():
    """Sign in user and return JSON Web Token to authenticate."""

    email = request.json['usr'].lower()
    pwd = request.json['pwd']
    usr_auth = q.search('auth','key', email)
    if usr_auth:
        # is user already signed up?
        pwd_hash = usr_auth[0][1]
    else:
        return jsonify({"status":"not found"}) 
    
    if pbkdf2_sha256.verify(pwd, pwd_hash):
        # if pwd verified, get account data and return with JSON web token
        payload = { 'email': email }
        token = jwt.generate_jwt(payload, JWT_security_key, 'PS256', timedelta(minutes=15))
        
        role = 'user'
        # role = 'user' unless 'admin'
        is_admin = q.search('admins', 'email', email)
        if len(is_admin) > 0:
            if is_admin[0][2] == 'admin':
                role = 'admin'
        
        response = jsonify({'jwt':token, 'status':'signed in', 'role':role})
        return response
    else:
        return jsonify({"status":"incorrect password"})

@app.route('/signup', methods = ['POST'])
def signup():
    """Sign up new user."""
    email = request.json['usr'].lower()
    pwd_hash = pbkdf2_sha256.hash(request.json['pwd'])
    row = ('email', 'pwd_hash', 'permission_time')
    row = (email, pwd_hash, datetime.now().strftime("%Y-%m-%d.%H:%M."))
    
    return jsonify(q.save_row_to_table('auth', row))

@app.route('/complete', methods = ['POST'])
def complete_profile():
    """Complete user profile."""
    
    token = request.json['token']
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        # search accounts table for email
        if len(q.search('accounts', 'key', email, precise=True)) == 0:
            # ie. there is no entry for this email
            now = datetime.now().strftime("%Y-%m-%d.%H:%M.")
            row = (email,\
                    now,\
                    request.json['mobilePhone'],\
                    email,\
                    request.json['postcode'],\
                    request.json['firstName'],\
                    request.json['lastName'],\
                    request.json['dateOfBirth'])
            
        
        
            return jsonify(q.save_row_to_table('accounts', row))
        else:
            return jsonify({'status':'error','detail':'profile already complete'})
    except:
        return jsonify({"status":'error','detail':'Invalid session token'})


@app.route('/profile/<token>', methods = ['GET'])
def profile(token):
    """Get profile data."""
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        # search accounts table for email
        
        # ~ return jsonify(len(q.search('accounts', 'email', email)))
        if len(q.search('accounts', 'key', email, precise=True)) == 0:
            return jsonify({'status':'no record'})
        else:
            return jsonify({'status':'record exists'})
        
    except:
        return jsonify({"status":'invalid session token'})

@app.route('/borrowed/<token>', methods = ['GET'])
def show_borrowed(token):
    """Get details of borrowed books for verified user."""
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        

    except:
        return jsonify({"status":'invalid session token'})
   
    transactions = q.search('transactions', 'account', email)
    borrowed = []
    requested = []
    for action in transactions:
        item_number = action[2]
        status = action[3]
        item_data = q.search('books', 'key', action[2], precise=True)[0]
        title = item_data[1]
        json_dict = {'item':item_number,'status':status,'title':title}
        if action[5] == "unfinished":
            if status == 'borrowed':
                borrowed.append(json_dict)
            if status == 'requested':
                requested.append(json_dict)

    return jsonify({'status':'success','borrowed':borrowed,'requested':requested})

if __name__ == "__main__":
    
    app.run()

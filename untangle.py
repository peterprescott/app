"""Script to untangle authors and publishers from book data."""

import csv
import os
import sys

import db_commands as db

q = db.Query(os.path.join(sys.path[0], 'auth.db'))

creator_books_cols = ('key', 'item_id', 'creator_id')
publisher_item_cols = ('key', 'item_id', 'publisher_id')
creator_cols = ('key', 'last_name', 'other_names')
publisher_cols = ('key', 'name')

q.drop_table('creator_books')
q.drop_table('publisher_item')
q.drop_table('creator')
q.drop_table('publishers')


q.create_table('creator_books', creator_books_cols)
q.create_table('publisher_item', publisher_item_cols)
q.create_table('creator', creator_cols)
q.create_table('publishers', publisher_cols)

with open(os.path.join(sys.path[0], 'orig-books.csv'), 'r', newline='') as csvfile:
    reader = csv.reader(csvfile, dialect='excel');
    i = 0
    for row in reader:
        if i > 0:
            book_id = str(i)
            print(f'Book ID is {book_id}')
            
            book_title = row[0]
            # [0'Title', 1'Format', 2'Author', 3'Publisher', 4'Date',\
            # 5'Pages', 6'Value', 7'DateValued', 8'ISBN', 9'Location', 10'Source', 11'Cost']
            
            ## first untangle the authors.
            
            # replace all known separators with semicolons
            author_list = row[2]
            semi_colons = author_list.replace(',',';')
            semi_colons = semi_colons.replace('&',';')
            semi_colons = semi_colons.replace('ed.',';')
            semi_colons = semi_colons.replace('trans.',';')
            semi_colons = semi_colons.replace('w/',';')
            semi_colons = semi_colons.replace('with',';')
            semi_colons = semi_colons.replace(' and ',';')
            
            # then split at semicolons
            authors = semi_colons.split(';')
            for author in authors:
                
                ## separate last_name from other_names
                space_split = author.split(' ')
                last_name = space_split.pop()
                other_names = ''
                
                for word in space_split:
                    other_names += ' ' + word
                last_name = last_name.strip()
                other_names = other_names.strip()
                print(last_name)
                strip_blank = last_name.replace(' ','')
                if len(strip_blank) == 0:
                    break
                # check if author is already saved
                match_creator = q.search('creator', 'last_name', last_name)
                print(match_creator)
                if len(match_creator) == 0:
                    # ie. no matches
                    creator_id = last_name + book_id
                    
                    new_creator = (creator_id, last_name, other_names)
                    
                    q.save_row_to_table('creator', new_creator)
                else:
                    # NB: I have obviously made a mistake here, because this is generative
                    if len(match_creator) == 1:
                        creator_id = match_creator[0][0]
                        
                    else:
                    # ie. possible matches
                        for match in match_creator:
                            print(f'\nAre these the same?\n{other_names + " " + last_name}\n{match[2] + " " + match[1]}\n(y)\n')
                            same = input()
                            if same == 'y':
                                creator_id=match[0]
                                break
                            else:
                                pass

                            
                # add creator_books
                
                new_label = (book_id, book_id, creator_id)
                q.save_row_to_table('creator_books', new_label)
            
            ## second, untangle the publishers.
            
            publisher = row[3]
            
            ## check if publisher is already saved
            
            match_publisher = q.search('publishers', 'name', publisher)
            print(match_publisher)
            if len(match_publisher) == 0:
                # ie. if not already saved
                print(book_id)
                publisher_id = book_id
                new_publisher = (publisher_id, publisher)            
                q.save_row_to_table('publishers', new_publisher)

            else:
                publisher_id = match_publisher[0][0]

            
            
            # add publisher_item
            
            publisher_label = (book_id, book_id, publisher_id)
            q.save_row_to_table('publisher_item', publisher_label)
            
            # ~ print(i-1, row[0])
        i += 1
    
q.dump_all()
